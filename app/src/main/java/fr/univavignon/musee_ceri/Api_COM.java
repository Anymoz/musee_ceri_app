package fr.univavignon.musee_ceri;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import Requetes.HttpCon;

//les scénarios avec l'API
public class Api_COM {

    public static boolean updateItem(Item item) throws IOException {
        URL searchItemUrl = WebServiceUrl.buildSearchItem(item.getWebId());
        String res = HttpCon.request(HttpCon.Type.GET, searchItemUrl.toString(), null, null);
        InputStream is = new ByteArrayInputStream(res.getBytes("UTF-8"));


        JSONResponseHandlerItem jsonItem = new JSONResponseHandlerItem(item);
        jsonItem.readJsonStream(is);
        item = jsonItem.getItem();

        return true;
    }

//    Actualiser la bdd locale avec le catalogue de l'API web
    public static ArrayList<Item> fetchAllItems() throws IOException {
        URL searchCatalogUrl = WebServiceUrl.buildSearchCatalog();
        String res = HttpCon.request(HttpCon.Type.GET, searchCatalogUrl.toString(), null, null);
        InputStream is = new ByteArrayInputStream(res.getBytes("UTF-8"));


        JSONResponseHandlerCatalog jsonCatalog = new JSONResponseHandlerCatalog();
        jsonCatalog.readJsonStream(is);
        return jsonCatalog.getItems();
    }

}
