package fr.univavignon.musee_ceri;

public enum Tri {
    ALPHABETS,
    CHRONO,
    CATEGORIES
}
