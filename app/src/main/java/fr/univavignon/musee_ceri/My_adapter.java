package fr.univavignon.musee_ceri;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static fr.univavignon.musee_ceri.SimpleSectionedRecyclerViewAdapter.*; //Section

public class My_adapter {

    public static SimpleSectionedRecyclerViewAdapter createAdapterCategories(Context context, List<String> categories, List<Item> catalog) {

        //    création des données et sections
        List<Item> itemsSortedByCategories = new ArrayList<>();
        List<Section> sections = new ArrayList<>();
        for(String str : categories) {
            sections.add(new Section(itemsSortedByCategories.size(), str));
            for(Item item : catalog) {
                if(item.getCategories().contains(str))
                    itemsSortedByCategories.add(item);
            }
        }

        RecyclerViewAdapter sectionlessAdapter = new RecyclerViewAdapter(context, itemsSortedByCategories);

        SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
        SimpleSectionedRecyclerViewAdapter adapter = new SimpleSectionedRecyclerViewAdapter(context,R.layout.section,R.id.section_text,sectionlessAdapter);
        adapter.setSections(sections.toArray(dummy));
        return adapter;
    }

    public static SimpleSectionedRecyclerViewAdapter createAdapterChronologique(Context context, List<Item> catalog) {

        //    création de RecyclerViewAdapter avec les données
        RecyclerViewAdapter sectionlessAdapter = new RecyclerViewAdapter(context, catalog);
        sectionlessAdapter.sortItemsChronologically();

        //    création des sections
        List<Section> sections = My_adapter.createSectionsChrono(sectionlessAdapter.catalog);


        SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
        SimpleSectionedRecyclerViewAdapter adapter = new SimpleSectionedRecyclerViewAdapter(context,R.layout.section,R.id.section_text,sectionlessAdapter);
        adapter.setSections(sections.toArray(dummy));
        return adapter;
    }

    private static List<Section> createSectionsChrono(List<Item> sortedCatalog) {
        List<Section> sections = new ArrayList<Section>();
        List<Integer> cache = new ArrayList<Integer>();
        for(Item item : sortedCatalog) {
            Integer year = item.getYear();
            if(!cache.contains(year)) {
                sections.add(new Section(sortedCatalog.indexOf(item), String.valueOf(year)));
                cache.add(year);
            }
        }
        return sections;
    }

    public static SimpleSectionedRecyclerViewAdapter createAdapterAlphabets(Context context, List<Item> catalog) {

    //    RecyclerViewAdapter avec les données
        RecyclerViewAdapter sectionAdapter = new RecyclerViewAdapter(context, catalog);
        sectionAdapter.sortItems();

    //    Création des sections
        List<Section> sections = My_adapter.createSections(sectionAdapter.catalog);


        SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
        SimpleSectionedRecyclerViewAdapter adapter = new SimpleSectionedRecyclerViewAdapter(context,R.layout.section,R.id.section_text,sectionAdapter);
        adapter.setSections(sections.toArray(dummy));
        return adapter;
    }

    private static List<Section> createSections(List<Item> sortedCatalog) {
        List<Section> sections = new ArrayList<Section>();
        List<String> cache = new ArrayList<String>();
        for(Item item : sortedCatalog) {
            String firstLetter = My_adapter.getFirstLetter(item.getName()).toUpperCase();
            if(!cache.contains(firstLetter)) {
                sections.add(new Section(sortedCatalog.indexOf(item), firstLetter));
                cache.add(firstLetter);
            }
        }
        return sections;
    }

    private static String getFirstLetter(String line) {
        Pattern p = Pattern.compile("\\p{L}");
        Matcher m = p.matcher(line);
        if (m.find())
            return m.group();
        return "?";
    }
}
